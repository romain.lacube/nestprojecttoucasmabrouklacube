import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Enclos } from '../enclos/enclos.entity';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity()
export class Animaux
{
   @ApiModelProperty()
   @PrimaryGeneratedColumn()
   id: number;

   @ApiModelProperty()
   @Column()
   name: string;

   @ApiModelProperty()
   @Column()
   carnivorous: boolean;

   @ApiModelProperty()
   @ManyToOne(type => Enclos, Enclos => Enclos.animaux)
   enclos: Enclos

}