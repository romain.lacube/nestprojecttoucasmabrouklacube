import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { AnimauxService } from '../services/animaux.service';
import { Animaux } from '../animaux.entity';
import { ApiImplicitQuery } from '@nestjs/swagger';

@Controller('animaux')
export class AnimauxController {

   constructor(private animauxService: AnimauxService) { }

   @Get()
   getAll(): Promise<Animaux[]>
   {
      return this.animauxService.findAll();
   }

   @Get(':id')
   getIndex(@Param('id') id): Promise<Animaux>
   {
      return this.animauxService.findOne(id);
   }

   @Post('create')
   async create(@Body() employeData: Animaux): Promise<any>
   {
      return this.animauxService.create(employeData);
   }

   @Put(':id/update')
   async update(@Param('id') id, @Body() employeData: Animaux): Promise<any>
   {
      employeData.id = Number(id);
      return this.animauxService.update(employeData);
   }

   @Delete(':id/delete')
   async delete(@Param('id') id): Promise<any>
   {
      return this.animauxService.delete(id);
   }

}