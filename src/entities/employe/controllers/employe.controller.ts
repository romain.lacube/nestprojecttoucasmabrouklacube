import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { EmployeService } from '../services/employe.service';
import { Employe } from '../employe.entity';

@Controller('employe')
export class EmployeController {

   constructor(private employeService: EmployeService) { }

   @Get()
   getAll(): Promise<Employe[]>
   {
      return this.employeService.findAll();
   }

   @Get(':id')
   getIndex(@Param('id') id): Promise<Employe>
   {
      return this.employeService.findOne(id);
   }

   @Post('create')
   async create(@Body() employeData: Employe): Promise<any>
   {
      return this.employeService.create(employeData);
   }

   @Put(':id/update')
   async update(@Param('id') id, @Body() employeData: Employe): Promise<any>
   {
      employeData.id = Number(id);
      return this.employeService.update(employeData);
   }

   @Delete(':id/delete')
   async delete(@Param('id') id): Promise<any>
   {
      return this.employeService.delete(id);
   }

}
