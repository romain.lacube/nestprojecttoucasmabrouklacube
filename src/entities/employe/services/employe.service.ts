import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employe } from '../employe.entity';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';

@Injectable()
export class EmployeService {
   constructor(
      @InjectRepository(Employe)
      private employeRepository: Repository<Employe>,
  ) { }

  
  async  findOne(id: number): Promise<Employe>
   {
      return await this.employeRepository.findOne(id);
   }

  async  findAll(): Promise<Employe[]>
   {
      return await this.employeRepository.find();
   }

   async  create(employe: Employe): Promise<Employe>
   {
      return await this.employeRepository.save(employe);
   }

   async update(employe: Employe): Promise<UpdateResult>
   {
      return await this.employeRepository.update(employe.id, employe);
   }

   async delete(id): Promise<DeleteResult>
   {
      return await this.employeRepository.delete(id);
   }
  
}
