import { Controller, Body, Post, Get, Param, Put, Delete } from '@nestjs/common';
import { EnclosService } from '../services/enclos.service';
import { Enclos } from '../enclos.entity';

@Controller('enclos')
export class EnclosController
{

   constructor(private enclosService: EnclosService) { }

   @Get()
   getAll(): Promise<Enclos[]>
   {
      return this.enclosService.findAll();
   }

   @Get(':id')
   getIndex(@Param('id') id): Promise<Enclos>
   {
      return this.enclosService.findOne(id);
   }

   @Post('create')
   async create(@Body() enclosData: Enclos): Promise<any>
   {
      return this.enclosService.create(enclosData);
   }

   @Put(':id/update')
   async update(@Param('id') id, @Body() enclosData: Enclos): Promise<any>
   {
      enclosData.id = Number(id);
      return this.enclosService.update(enclosData);
   }

   @Delete(':id/delete')
   async delete(@Param('id') id): Promise<any>
   {
      return this.enclosService.delete(id);
   }
}
