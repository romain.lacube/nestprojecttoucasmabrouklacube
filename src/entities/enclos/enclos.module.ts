import { Module } from '@nestjs/common';
import { EnclosController } from './controllers/enclos.controller';
import { EnclosService } from './services/enclos.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Enclos } from './enclos.entity';

@Module({
   imports: [
      TypeOrmModule.forFeature([
         Enclos
      ]),
    ],
  controllers: [EnclosController],
  providers: [EnclosService]
})
export class EnclosModule {}
