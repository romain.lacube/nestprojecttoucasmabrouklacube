import { Controller, Get } from '@nestjs/common';
import { ZooService } from '../services/zoo.service';
import { Zoo } from '../zoo.entity';
import { Post,Put, Delete, Body, Param } from  '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';

@Controller('zoo')
export class ZooController {

   constructor(private zooService: ZooService){}

  
   @Get()
   getIndex():  Promise<Zoo[]>
   {
     return this.zooService.findAll();
   }

   @Get(':id')
   getIndexOne(@Param('id') id): Promise<Zoo>
   {
      return this.zooService.findOne(id);
   }

   @Post('create')
    async create(@Body() zooData: Zoo): Promise<any> {
      return this.zooService.create(zooData);
    }
    
    @Put(':id/update')
    async update(@Param('id') id, @Body() zooData: Zoo): Promise<any> {
        zooData.id = Number(id);
        console.log('Update #' + zooData.id)
        return this.zooService.update(zooData);
    }

    @Delete(':id/delete')
    async delete(@Param('id') id): Promise<any> {
      return this.zooService.delete(id);
    }  

}
