import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn } from 'typeorm';
import { Employe } from "../employe/employe.entity";
import { Enclos } from '../enclos/enclos.entity';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity()
export class Zoo {

    @ApiModelProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ApiModelProperty()
    @Column()
    name: string;

    @ApiModelProperty()
    @Column()
    location: string;

    @ApiModelProperty()
    @Column()
    postal_code: number;

    @ApiModelProperty()
    @OneToMany(type => Employe, Employe => Employe.zoo) // note: we will create author property in the Photo class below
    employe: Employe[];
    
    @ApiModelProperty()
    @OneToMany(type => Enclos, Enclos => Enclos.zoo) // note: we will create author property in the Photo class below
    enclos: Enclos[];

    

}