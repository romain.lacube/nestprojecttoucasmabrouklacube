import { Module } from '@nestjs/common';
import { ZooController } from './controllers/zoo.controller';
import { ZooService } from './services/zoo.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Zoo } from './zoo.entity';

@Module({
   imports: [
      TypeOrmModule.forFeature([
         Zoo
      ]),
    ],
  providers: [ZooService],
  controllers: [ZooController]
})
export class ZooModule {}
