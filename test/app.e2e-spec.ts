import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ZooModule } from './../src/entities/zoo/zoo.module';
import { Zoo } from './../src/entities/zoo/zoo.entity';
import { EnclosModule } from './../src/entities/enclos/enclos.module';
import { Enclos } from './../src/entities/enclos/enclos.entity';
import { EmployeModule } from './../src/entities/employe/employe.module';
import { Employe } from './../src/entities/employe/employe.entity';
import { AnimauxModule } from './../src/entities/animaux/animaux.module';

let zoo;
let enclos;
let employe;
let animaux;

describe('ZooController(e2e)', () =>
{
   let app;

   beforeAll(async () =>
   {
      const moduleFixture: TestingModule = await Test.createTestingModule({
         imports: [TypeOrmModule.forRoot(), ZooModule],
      })
         .compile();

      app = moduleFixture.createNestApplication();
      await app.init();
   });

   it('/zoo (GET All zoo)', async () =>
   {
      const response = await request(app.getHttpServer())
         .get('/zoo');
      expect(response.status).toBe(200);
   });

   it('/zoo/create (POST)', async () =>
   {
      const response = await request(app.getHttpServer())
         .post('/zoo/create')
         .send({ name: 'test', location: 'test', postal_code: '99999' });
      zoo = response["body"];
      expect(response.status).toBe(201);
   });

   it('/:id/update (UPDATE)', async () =>
   {
      const response = await request(app.getHttpServer())
         .put('/zoo/' + zoo["id"] + '/update')
         .send({ name: 'nouveau nom ', location: 'nouvelle localisation' });
      expect(response.status).toBe(200);
   });

   it('/zoo/id (GET one zoo)', async () =>
   {
      const response = await request(app.getHttpServer())
         .get('/zoo/' + zoo["id"])
      expect(response.status).toBe(200);

   });

   it(':id/delete (delete one zoo)', async () =>
   {
      const response = await request(app.getHttpServer())
         .delete('/zoo/' + zoo["id"] + '/delete')
      expect(response.status).toBe(200);

   });


   // @Delete(':id/delete')
   // async delete(@Param('id') id): Promise<any> {
   //   return this.zooService.delete(id);
   // }  




   afterAll(async () =>
   {
      await app.close();
   });
});

describe('EnclosController(e2e)', () =>
{
   let app;

   beforeAll(async () =>
   {
      const moduleFixture: TestingModule = await Test.createTestingModule({
         imports: [TypeOrmModule.forRoot(), EnclosModule],
      })
         .compile();

      app = moduleFixture.createNestApplication();
      await app.init();
   });

   it('/enclos (GET)', async () =>
   {
      const response = await request(app.getHttpServer())
         .get('/enclos');
      expect(response.status).toBe(200);
   });

   it('/enclos/create (POST)', async () =>
   {
      const response = await request(app.getHttpServer())
         .post('/enclos/create')
         .send({ name: 'test', location: 'test', tailleMaxAnimaux: '99999', zooId: zoo["id"] });
      enclos = response["body"];
      expect(response.status).toBe(201);

   });

   it('enclos/:id(GET)', async () =>
   {
      const response = await request(app.getHttpServer())
         .get('/enclos/' + enclos["id"]);
      expect(response.status).toBe(200);

   });

   it('enclos/:id/update(PUT)', async () =>
   {
      const response = await request(app.getHttpServer())
         .put('/enclos/' + enclos["id"] + '/update')
         .send({ name: 'test' });
      expect(response.status).toBe(200);

   });

   it('enclos/:id/delete(DELETE)', async () =>
   {
      const response = await request(app.getHttpServer())
         .delete('/enclos/' + enclos["id"] + '/delete');
      expect(response.status).toBe(200);

   });


   afterAll(async () =>
   {
      await app.close();
   });
});

describe('EmployeController(e2e)', () =>
{
   let app;

   beforeAll(async () =>
   {
      const moduleFixture: TestingModule = await Test.createTestingModule({
         imports: [TypeOrmModule.forRoot(), EmployeModule],
      })
         .compile();

      app = moduleFixture.createNestApplication();
      await app.init();
   });

   it('/employe (GET)', async () =>
   {
      const response = await request(app.getHttpServer())
         .get('/employe');
      expect(response.status).toBe(200);
   });

   it('/employe/create (POST)', async () =>
   {
      const response = await request(app.getHttpServer())
         .post('/employe/create')
         .send({ lastName: 'test', firstName: 'test', location: 'test', zooId: zoo["id"] });
      employe = response["body"];
      expect(response.status).toBe(201);

   });

   it('employe/:id(GET)', async () =>
   {
      const response = await request(app.getHttpServer())
         .get('/employe/' + employe["id"]);
      expect(response.status).toBe(200);

   });

   it('employe/:id/update(PUT)', async () =>
   {
      const response = await request(app.getHttpServer())
         .put('/employe/' + employe["id"] + '/update')
         .send({ lastName: 'test' });
      expect(response.status).toBe(200);

   });

   it('employe/:id/delete(DELETE)', async () =>
   {
      const response = await request(app.getHttpServer())
         .delete('/employe/' + employe["id"] + '/delete');
      expect(response.status).toBe(200);

   });


   afterAll(async () =>
   {
      await app.close();
   });
});

describe('AnimauxController(e2e)', () =>
{
   let app;

   beforeAll(async () =>
   {
      const moduleFixture: TestingModule = await Test.createTestingModule({
         imports: [TypeOrmModule.forRoot(), AnimauxModule],
      })
         .compile();

      app = moduleFixture.createNestApplication();
      await app.init();
   });

   it('/animaux (GET)', async () =>
   {
      const response = await request(app.getHttpServer())
         .get('/animaux');
      expect(response.status).toBe(200);
   });

   it('/animaux/create (POST)', async () =>
   {
      const response = await request(app.getHttpServer())
         .post('/animaux/create')
         .send({ name: 'test', carnivorous: true, enclosId: enclos["id"] })
      animaux = response["body"]
      expect(response.status).toBe(201);

   });

   it('animaux/:id(GET)', async () =>
   {
      const response = await request(app.getHttpServer())
         .get('/animaux/' + animaux["id"]);
      expect(response.status).toBe(200);

   });

   it('animaux/:id/update(PUT)', async () =>
   {
      const response = await request(app.getHttpServer())
         .put('/animaux/' + animaux["id"] + '/update')
         .send({ name: 'nouvel animal' });
      expect(response.status).toBe(200);

   });

   it('animaux/:id/delete(DELETE)', async () =>
   {
      const response = await request(app.getHttpServer())
         .delete('/animaux/' + animaux["id"] + '/delete');
      expect(response.status).toBe(200);

   });


   afterAll(async () =>
   {
      await app.close();
   });
});
